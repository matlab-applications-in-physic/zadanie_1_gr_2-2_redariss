'task1 calculate h/(2*pi)'
h=6.62607015e-34; %planck constant from NIST
h/(2*pi)
'task2 calculate sin(30�/e), where e is Euler number'
sind(30/exp(1))%function sind, used to calculate in degrees,exp(1) used to get e
'task3 calculate 0c00123d3/(2.455*1E23)'
a=hex2dec('0c00123d3'); %function hex2dec('HEX') returns decimal value of hexadecimal string 
a/(2.455e23)
'task4 calculate sqrt(e-pi), where e is Euler number'
sqrt(exp(1)-pi)
'task5 display 10th character after the decimal mark of pi number'
zmiennaPi=pi*1e10;%multipling pi by 1E10 to "move" comma by 10 places 
floor(mod(zmiennaPi,10))%mod(x,y) gives rest from dividing x by y, so i divided zmiennaPi by 10 to get 10th number as an integer part in a rest, next I used floor to get only the integer
'task6 display number of days since your birth'
T2=[2019 10 18 10 48 0]; %current date
T1=[1999 03 17 7 0 0]; %date of birth
etime(T2,T1)/(3600*24)%calculates number of seconds between 2 dates, divided by numbers of second in one day

'task7 arctg((exp(sqrt(7)/2-ln(Rz/1e5))/0caabb), where Rz is Earth radius'
rZiemi= 6.3781e6;%decalring Earth radius from Wikipedia
atan(exp(sqrt(7)/2-log(rZiemi/1e5))/hex2dec('0caabb'))%function log is ln
'task8 number of atoms in 1/5 micromol of alcohol'
nA=6.02214076e23%declarin Avogardo number from NIST
1/5*nA*9*1e-6 %9 is number of atoms in alcohol molecule
'task9 number of atoms C13 in 1/5 micromol of alcohol'
1/5*nA*9*1e-6*2/9*0.01%2/9 is number of C atoms in alocohol molecule, 0.01 is number of C13 atoms per one C12 atom





